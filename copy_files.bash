#!/usr/bin/bash


cp ~/.i3/config .i3
echo "Copied config file"

cp ~/.i3/i3blocks.conf .i3
echo "Copied i3blocks config file"

cp -R ~/.i3/scripts .i3/scripts
echo "Copied .i3/scripts"
